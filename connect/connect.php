<?php
  // Replace these variables with your database credentials
  $host = "localhost";       // Replace with your actual database host
  $dbname = "hotel";     // Replace with your actual database name
  $username = "root";   // Replace with your actual database username
  $password = "";   // Replace with your actual database password

  try {
      // Create a new PDO instance
      $pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);

      // Set the PDO error mode to exception
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      // Return the PDO object
      return $pdo;
  } catch (PDOException $e) {
      // If there's an error, you can handle it here
      die("Connection failed: " . $e->getMessage());
  }
?>



