<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HongSabye | RoomDetail</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Option 1: Include in HTML -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
    integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.0/dist/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css">
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <link rel="stylesheet" href="assets/style.css">
</head>
<body>
    <?php include 'navbar.php';?>

    <div class="container mt-5">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-10">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <h3 class="text-roomdetail-1"></h3>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-3">
                        <h5 class="mt-1 text-roomdetail-2"></h5>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-9">
                        <small class="font-13 text-roomdetail-3 d-none">จำนวนคนเข้าดู 73 คน</small>
                    <small class="font-13 text-roomdetail-3 ms-4 d-none">โพสต์โดย <span class="ms-1">admin admin</span></small>
                    <small class="font-13 text-roomdetail-3 ms-4 d-none">โพสต์วันที่ 23 สิงหาคม 2566</small>
                    </div>
                    
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-2 mb-4 mt-2 ">
                <button type="button" class="btn btn-dark btn-sell w-100"><p class="mb-0 roomTypeMonthlyRental">aa</p></button>
            </div>
        </div>
       

        <div class="room-detail">
            <div class="row align-items-center h-100">
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="card border-0 rounded">
                        <div class="card-body p-0">
                            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                                <div class="carousel-inner" id="carousel-item">
                                    <div class="lazyload" style="border-radius: 5px; max-height: 450px; width: 100%; height: 20vw; margin-right: 10px;"></div>
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>
                            <!-- <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-4 card-room">
                                    <img class="w-100 h-100 img-room rounded" src="assets/img/room/room-1.webp" alt="">
                                </div>
                                <div class="col-8 ps-0 card-room d-none d-sm-none d-md-none d-lg-flex">
                                    <div class="row">
                                        <div class="col-6 mb-2 card-room-2">
                                            <img class="w-100 h-100 img-room rounded" src="assets/img/room/room-2.webp" alt="">
                                        </div>
                                        <div class="col-6 ps-0 mb-2 card-room-2">
                                            <img class="w-100 h-100 img-room rounded" src="assets/img/room/room-3.webp" alt="">
                                        </div>
                                        <div class="col-6 mb-2 card-room-2">
                                            <img class="w-100 h-100 img-room rounded" src="assets/img/room/room-4.jpeg" alt="">
                                        </div>
                                        <div class="col-6 ps-0 mb-2 card-room-2">
                                            <img class="w-100 h-100 img-room rounded" src="assets/img/room/room-5.webp" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <a href="#" id="btn-see-allImg" data-fancybox="images" type="button" class="btn btn-primary position-absolute top-0 end-0 mt-3 me-3"><i class="fas fa-images me-2"></i> ดูรูปทั้งหมด</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 ps-5 mt-4 pos-roomdetail-2">
                    <p class="text-detail-1"> <i class="fas fa-info-circle me-2 ms-2"></i> รายละเอียดห้อง</p>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 mt-4">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <h6>ค่าห้อง</h6>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <p class="text-end roomTypeMonthlyRental mb-0"></p>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <h6>ระยะสัญญา</h6>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <p class="text-end mb-0" id="service"></p>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <h6>ค่าไฟ</h6>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <p class="text-end mb-0" id="electricitybill"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="row pos-roomdetail-1">
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <h6>ค่ามัดจำ</h6>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <p class="text-end mb-0" id="guarantee"></p>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <h6>ขนาดห้อง</h6>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <p class="text-end mb-0" id="roomTypeSize"></p>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <h6>ค่าน้ำ</h6>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <p class="text-end mb-0" id="dataWaterbill"></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="text-detail-1 mt-4"><i class="fas fa-info-circle me-2 ms-2"></i>จุดเด่น</p>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-6 mt-4">
                            <div class="d-flex">
                                <i class="fas fa-car font-50 me-3"></i>
                                <div class="row">
                                    <h5>มีสถานที่จอดรถ</h5>
                                    <p>มีสถานที่สำหรับจอดรถยนต์</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 mt-4">
                            <div class="d-flex">
                                <i class="fas fa-subway font-50 me-3"></i> 
                                <div class="row">
                                    <h5>ติดสถานีรถไฟฟ้า</h5>
                                    <p>รถไฟฟ้าอยู่หน้าที่พัก สะดวกต่อการเดินทาง</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 mt-2">
                            <div class="d-flex">
                                <i class="fas fa-map font-50 me-3"></i>
                                <div class="row">
                                    <h5>ไกล้ศูนย์การค้าหรือห้างสรรพสินค้า</h5>
                                    <p>สะดวกต่อการซื้อของอุปโภคบริโภค</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 mt-2">
                            <div class="d-flex">
                                <i class="fas fa-user-shield font-50 me-3"></i> 
                                <div class="row">
                                    <h5>ระบบรักษาความปลอดภัย</h5>
                                    <p>มีระบบคีการ์ด หรือผู้รักษาความปลอดภัย</p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    <section>
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-8">
                    <p class="text-detail-1"><i class="fas fa-info-circle me-2 ms-2"></i>ได้อะไรบ้างจากห้องนี้ ?</p>
                    <div class="row mt-5 pos-roomdetail-2" id="facilities">
                        
                        <!-- <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-chair me-2"></i> ชุดโต๊ะ เก้าอี้</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-couch me-2"></i> โซฟา / โซฟาปรับนอน</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-tv me-2"></i> โทรทัศน์</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-snowflake me-2"></i> ตู้เย็น</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-tshirt me-2"></i> ตู้เสื้อผ้า</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-snowflake me-2"></i> แอร์</div> 
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-sink me-2"></i> ซิงค์ล้างจาน</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-tv me-2"></i> ไมโครเวฟ</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-soap me-2"></i>เครื่องซักผ้า</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-credit-card me-2"></i>ระบบคีย์การ์ด</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="far fa-caret-square-up me-2"></i> ลิฟต์</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-running me-2"></i>ฟิตเนส</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-car me-2"></i>ที่จอดรถยนต์</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-motorcycle me-2"></i> ที่จอดรถมอเตอร์ไซ</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-street-view me-2"></i>ลานหรือระเบียง</div>
                        <div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-eye me-2"></i> กล้องวงจรปิด</div> -->
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4 mb-4">
                    <div class="card border-0 shadow-lg" style="border-radius: 15px; margin-top: 80px;">
                        <div class="card-body">
                            <h5 class="card-title roomTypeMonthlyRental">บาท</h5>
                            <!-- <div class="mb-3 mt-4">
                                <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="สอบถามเพิ่มเติม">
                            </div>
                            <button type="button" class="btn btn-primary w-100"><i class="fas fa-paper-plane me-2"></i> ส่งข้อความ</button> -->

                            <h5 class="mt-4">ติดต่อเพิ่มเติม</h5>
                            <p class="pb-0 mb-0"><i class="fas fa-phone-alt me-2"></i> โทรศัพท์ : <span id="contactInformationTel"></span> </p>
                            <p class="pb-0 mb-0"><i class="fab fa-line me-2"></i> ไลน์ไอดี : <span id="contactInformationLineID"></span>  </p>
                            <p class="pb-0 mb-0"><i class="fab fa-facebook-square me-2"></i> เฟซบุ๊ค  : <span id="contactInformationName"></span> </p>
                            <p class="pb-0 mb-0"><i class="fas fa-envelope me-2"></i> อีเมล   : <span id="contactInformationEmail"></span>  </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container mt-5">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6 mb-4">
                    <p class="text-detail-1 mb-4"><i class="fas fa-map me-3 ms-3"></i> สถานที่ / ที่อยู่</p>
                    <h5 class="mb-4"><span id="detail">Knightsbridge Phaholyothin-Interchange 12 ซอย พหลโยธิน 59 Anusawaree เขตบางเขน กรุงเทพมหานคร 10220
</span></h5>
                    <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=KNIGHTSBRIDGE%20PHAHOLYOTHIN%20-%20INTERCHANGE&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}</style><a href="https://www.embedgooglemap.net"></a><style>.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div></div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 mb-4">
                    <p class="text-detail-1 mb-4"><i class="fas fa-info-circle me-3 ms-3"></i> รายละเอียดเพิ่มเติม</p>
                    <p id="details"></p>
                </div>
            </div>
        </div>
    </section>

    <div class="photo-gallery d-none mt-4 mb-3">
        <div class="container">
            <div class="row photos">
                <p class="imglist" id="fancybox-img">
                    <!-- <div class="col-sm-12 col-md-12 col-lg-12 mx-auto position-relative item"> <a href="assets/img/room/room-2.webp" data-fancybox="images"><p class="type-news-detail text-white pl-2 pr-2 ml-3 mt-3">ดูรูปภาพทั้งหมด</p><img class="img-lightbox" src="assets/img/room/room-2.webp" /></a></div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mx-auto position-relative item"> <a href="assets/img/room/room-3.webp" data-fancybox="images"><p class="type-news-detail text-white pl-2 pr-2 ml-3 mt-3">ดูรูปภาพทั้งหมด</p><img class="img-lightbox" src="assets/img/room/room-3.webp" /></a></div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mx-auto position-relative item"> <a href="assets/img/room/room-4.jpeg" data-fancybox="images"><p class="type-news-detail text-white pl-2 pr-2 ml-3 mt-3">ดูรูปภาพทั้งหมด</p><img class="img-lightbox" src="assets/img/room/room-4.jpeg" /></a></div>
                    <div class="col-sm-12 col-md-12 col-lg-12 mx-auto position-relative item"> <a href="assets/img/room/room-5.webp" data-fancybox="images"><p class="type-news-detail text-white pl-2 pr-2 ml-3 mt-3">ดูรูปภาพทั้งหมด</p><img class="img-lightbox" src="assets/img/room/room-5.webp" /></a></div> -->
                </p>                    
            </div>
        </div>
    </div>


    <?php include 'footer.php';?>
    
   <script src="assets/js/pages/roomdetail.js"></script>

</body>
</html>