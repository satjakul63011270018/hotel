<?php
if (isset($_FILES['photos'])) {
    $acc_id = $_POST['acc_id'];

    $uploaded_images = [];
    $upload_failed = false;

    $allowed_extensions = ["jpg", "jpeg", "png"];
    $upload_dir = 'uploads/';

    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "hotel";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $random_string = '';
        for ($i = 0; $i < $length; $i++) {
            $random_string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $random_string;
    }

    foreach ($_FILES['photos']['tmp_name'] as $key => $tmp_name) {
        $file_name = $_FILES['photos']['name'][$key];
        $file_tmp = $_FILES['photos']['tmp_name'][$key];

        $file_extension = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
        if (!in_array($file_extension, $allowed_extensions)) {
            echo "Invalid file extension for '$file_name'. Only JPG, JPEG, and PNG files are allowed.<br>";
            $upload_failed = true;
            continue;
        }

        $random_string = generateRandomString(10);
        $new_file_name = time() . '_' . $random_string . '.' . $file_extension;
        $upload_path = $upload_dir . $new_file_name;

        if (!move_uploaded_file($file_tmp, $upload_path)) {
            echo "Failed to upload '$new_file_name'.<br>";
            $upload_failed = true;
            continue;
        }
        $uploaded_images[] = $upload_path;
    }

    if (!$upload_failed) {
        foreach ($uploaded_images as $path) {
            $sql = "INSERT INTO `image` (`id`, `acc_id`, `path`) VALUES (NULL, $acc_id, '$path')";
            if ($conn->query($sql) === false) {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }

        $conn->close();

        echo "Images uploaded and inserted into the database successfully.";
    } else {
        echo "One or more uploads failed. Please check the errors above.";
    }
} else {
    echo "No images were uploaded.";
}
?>
