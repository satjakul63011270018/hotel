<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hotel";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $query = "SELECT id, accommodationName, province, amphure, roomTypeMonthlyRental FROM accommodations ORDER BY `accommodations`.`id` DESC LIMIT 8 ";
    $stmt = $conn->query($query);
    $accommodations = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($accommodations as &$accommodation) {
        $accId = $accommodation['id'];
        $imageQuery = "SELECT * FROM `image` WHERE acc_id = ?";
        $imageStmt = $conn->prepare($imageQuery);
        $imageStmt->execute([$accId]);
        $images = $imageStmt->fetchAll(PDO::FETCH_ASSOC);
        $accommodation['images'] = $images;
    }

    // Return JSON response
    echo json_encode($accommodations);
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>
