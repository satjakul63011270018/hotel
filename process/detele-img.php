<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hotel";

try {
    // Create a PDO connection
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // Set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $postData = $_POST["data"];


    // Prepare the SQL DELETE query
    $sql = "DELETE FROM image WHERE id = :idToDelete";

    // Prepare the statement
    $stmt = $conn->prepare($sql);

    // Bind parameter and execute the DELETE statement
    $stmt->bindParam(':idToDelete', $postData, PDO::PARAM_INT);

    if ($stmt->execute()) {
        // Set HTTP 200 response for successful deletion
        http_response_code(200);
        echo "Record with ID $postData has been deleted successfully.";
    } else {
        // Set HTTP 500 response for server error
        http_response_code(500);
        echo "Error deleting record.";
    }

} catch (PDOException $e) {
    // Handle database connection errors
    echo "Connection failed: " . $e->getMessage();
}

?>
