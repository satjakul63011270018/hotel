<?php
// Establish PDO connection
session_start();
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hotel";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname;charset=utf8", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Get the data from the POST request
    $postData = $_POST["data"];

    // Query to select all data from the table
    $sql = "SELECT id, accommodationName, province, amphure, roomTypeMonthlyRental FROM accommodations WHERE username = :postData";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':postData', $postData, PDO::PARAM_STR);
    $stmt->execute();

    // Fetch all rows as associative arrays
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Loop through the fetched rows and fetch associated images for each row
    foreach ($rows as &$row) {
        $accId = $row['id'];
        $sql2 = "SELECT * FROM `image` WHERE acc_id = :accId";
        $stmt2 = $conn->prepare($sql2);
        $stmt2->bindParam(':accId', $accId, PDO::PARAM_INT);
        $stmt2->execute();
        $images = $stmt2->fetchAll(PDO::FETCH_ASSOC);
        $row['images'] = $images;
    }

    // Return the data as JSON
    header('Content-Type: application/json');
    echo json_encode($rows);
} catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}

// Close the connection
$conn = null;
?>
