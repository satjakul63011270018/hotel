<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hotel";

// Create a connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Prepare the SQL insert query
$sql = "INSERT INTO `accommodations` (`id`, `username`, `accommodationName`, `addressNumber`, `addressRoad`, `addressAlley`, `province`, `amphure`, `tambon`, `zipCode`, `facilities`, `roomTypeName`, `roomTypeLayout`, `roomTypeSize`, `roomTypeMonthlyRental`, `roomTypeDailyRental`, `roomTypeStatus`, `dataWaterbill`, `electricitybill`, `service`, `guarantee`, `phonebill`, `Internet`, `details`, `contactInformationName`, `contactInformationTel`, `contactInformationEmail`, `contactInformationLineID`) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

// Prepare the statement
$stmt = $conn->prepare($sql);

if ($stmt) {
    // Bind parameters and execute the statement
    $stmt->bind_param(
        "sssssssssssssssssssssssssss",
        $_POST["data"]["username"],
        $_POST["data"]["accommodationName"],
        $_POST["data"]["addressNumber"],
        $_POST["data"]["addressRoad"],
        $_POST["data"]["addressAlley"],
        $_POST["data"]["province"],
        $_POST["data"]["amphure"],
        $_POST["data"]["tambon"],
        $_POST["data"]["zipCode"],
        $_POST["data"]["facilities"]["isCheck"],
        $_POST["data"]["roomTypeName"],
        $_POST["data"]["roomTypeLayout"],
        $_POST["data"]["roomTypeSize"],
        $_POST["data"]["roomTypeMonthlyRental"],
        $_POST["data"]["roomTypeDailyRental"],
        $_POST["data"]["roomTypeStatus"],
        $_POST["data"]["dataWaterbill"],
        $_POST["data"]["electricitybill"],
        $_POST["data"]["service"],
        $_POST["data"]["guarantee"],
        $_POST["data"]["phonebill"],
        $_POST["data"]["Internet"],
        $_POST["data"]["details"],
        $_POST["data"]["contactInformationName"],
        $_POST["data"]["contactInformationTel"],
        $_POST["data"]["contactInformationEmail"],
        $_POST["data"]["contactInformationLineID"]
    );

    if ($stmt->execute()) {
        // Get the inserted ID
        $insertedId = $stmt->insert_id;

        // Retrieve the inserted data from the database
        $selectSql = "SELECT id FROM `accommodations` WHERE `id` = ?";
        $selectStmt = $conn->prepare($selectSql);
        $selectStmt->bind_param("i", $insertedId);
        $selectStmt->execute();
        $result = $selectStmt->get_result();
        $insertedData = $result->fetch_assoc();

        // Close the select statement
        $selectStmt->close();

        // Return the inserted data as JSON
        echo json_encode($insertedData);
    } else {
        echo "Error: " . $stmt->error;
    }

    // Close the statement
    $stmt->close();
} else {
    echo "Error: " . $conn->error;
}

// Close the connection
$conn->close();
?>
