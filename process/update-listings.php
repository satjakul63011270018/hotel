
<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hotel";

// Create a connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$postData = $_POST["data"];

// Prepare the SQL update query
$sql = "UPDATE `accommodations` SET
            `username` = ?,
            `accommodationName` = ?,
            `addressNumber` = ?,
            `addressRoad` = ?,
            `addressAlley` = ?,
            `province` = ?,
            `amphure` = ?,
            `tambon` = ?,
            `zipCode` = ?,
            `facilities` = ?,
            `roomTypeName` = ?,
            `roomTypeLayout` = ?,
            `roomTypeSize` = ?,
            `roomTypeMonthlyRental` = ?,
            `roomTypeDailyRental` = ?,
            `roomTypeStatus` = ?,
            `dataWaterbill` = ?,
            `electricitybill` = ?,
            `service` = ?,
            `guarantee` = ?,
            `phonebill` = ?,
            `Internet` = ?,
            `details` = ?,
            `contactInformationName` = ?,
            `contactInformationTel` = ?,
            `contactInformationEmail` = ?,
            `contactInformationLineID` = ?
         WHERE `id` = ?";

// Prepare the statement
$stmt = $conn->prepare($sql);

if ($stmt) {
    // Extract the "id" field from the postData array
    $id = $postData["id"];

    // Bind parameters and execute the update statement
    $stmt->bind_param(
        "sssssssssssssssssssssssssssi",
        $postData["username"],
        $postData["accommodationName"],
        $postData["addressNumber"],
        $postData["addressRoad"],
        $postData["addressAlley"],
        $postData["province"],
        $postData["amphure"],
        $postData["tambon"],
        $postData["zipCode"],
        $postData["facilities"]["isCheck"],
        $postData["roomTypeName"],
        $postData["roomTypeLayout"],
        $postData["roomTypeSize"],
        $postData["roomTypeMonthlyRental"],
        $postData["roomTypeDailyRental"],
        $postData["roomTypeStatus"],
        $postData["dataWaterbill"],
        $postData["electricitybill"],
        $postData["service"],
        $postData["guarantee"],
        $postData["phonebill"],
        $postData["Internet"],
        $postData["details"],
        $postData["contactInformationName"],
        $postData["contactInformationTel"],
        $postData["contactInformationEmail"],
        $postData["contactInformationLineID"],
        $id
    );

    if ($stmt->execute()) {
        // Close the update statement
        $stmt->close();

        // Prepare and execute the SELECT query
        $selectSql = "SELECT id FROM `accommodations` WHERE `id` = ?";
        $selectStmt = $conn->prepare($selectSql);
        $selectStmt->bind_param("i", $id);
        $selectStmt->execute();
        $result = $selectStmt->get_result();

        if ($result->num_rows > 0) {
            // Fetch the result
            $row = $result->fetch_assoc();

            // Create a JSON response
            $response = array("id" => $row["id"]);

            // Close the SELECT statement
            $selectStmt->close();

            // Set HTTP 200 response and return the JSON data
            http_response_code(200);
            header("Content-Type: application/json");
            echo json_encode($response);
        } else {
            // No rows found
            http_response_code(404);
        }
    } else {
        // Set HTTP 500 response for server error
        http_response_code(500);
    }
} else {
    echo "Error: " . $conn->error;
}

// Close the connection
$conn->close();
?>
