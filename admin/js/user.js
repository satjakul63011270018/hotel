const dataTable = $("#table-user").DataTable({
  ordering: false,
  data: [],
  columns: [
    {
      title: "ชื่อ",
      render: function (data, type, row) {
        return `<p>${row.first_name}</p>`;
      },
    },
    {
      title: "นามสกุล",
      render: function (data, type, row) {
        return `<p>${row.last_name}</p>`;
      },
    },
    {
      title: "Email",
      className: "text-center",
      render: function (data, type, row) {
        return `
            <p>${row.email}</p>`;
      },
      orderable: false,
    },
    {
      title: "เครื่องมือ",
      className: "text-center",
      render: function (data, type, row) {
        return `
        <button type="button" class="btn btn-danger text-white" onclick="deteleListings(${row.id})">ลบ</button>`;
      },
      orderable: false,
    },
  ],
});

const createDataTable = (data) => {
  dataTable.clear();
  dataTable.rows.add(data ?? []).draw();
};
async function fetchListings() {
  try {
    const response = await $.ajax({
      type: "get",
      url: "http://localhost/hotel/process/get-alluser.php",
    });
    let res = JSON.parse(response);
    console.log(res);
    createDataTable(res);
  } catch (error) {
    console.error(error);
  }
}

async function deteleListings(data) {
  try {
    $.ajax({
      type: "POST",
      url: "http://localhost/hotel/process/detele-listings.php",
      data: { data },
      success: function (response) {
        location.reload();
      },
      error: function (error) {
        console.error(error);
      },
    });
  } catch (error) {
    console.error(error);
  }
}

$(document).ready(async function () {
  await fetchListings();
});
