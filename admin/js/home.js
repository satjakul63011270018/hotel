const dataTable = $("#basic-2").DataTable({
  ordering: false,
  data: [],
  columns: [
    {
      title: "ชื่อหอพัก",
      render: function (data, type, row) {
        return `<p>${row.accommodationName}</p>`;
      },
    },
    {
      title: "ที่อยู่",
      render: function (data, type, row) {
        return `<p>${row.province}, ${row.amphure}</p>`;
      },
    },
    {
      title: "สถานะ",
      className: "text-center",
      render: function (data, type, row) {
        return `
            <p>${row.roomTypeStatus}</p>`;
      },
      orderable: false,
    },
    {
      title: "เครื่องมือ",
      className: "text-center",
      render: function (data, type, row) {
        return `
        <a href="/hotel/members/listings.php?room=${row.id}&type=edit" type="button" class="btn btn-warning text-white me-3">แก้ไขห้องพัก</a>
        <button type="button" class="btn btn-danger text-white" onclick="deteleListings(${row.id})">ลบห้องพัก</button>`;
      },
      orderable: false,
    },
  ],
});

const createDataTable = (data) => {
  dataTable.clear();
  dataTable.rows.add(data ?? []).draw();
};
async function fetchListings() {
  try {
    const response = await $.ajax({
      type: "get",
      url: "http://localhost/hotel/process/get-allroom.php",
    });
    let res = JSON.parse(response);
    // console.log(res);
    createDataTable(res);
  } catch (error) {
    console.error(error);
  }
}

async function deteleListings(data) {
  Swal.fire({
    title: "คุณแน่ใจไหม?",
    text: "คุณจะเปลี่ยนกลับไม่ได้!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "ยืนยะน",
  }).then((result) => {
    if (result.isConfirmed) {
      try {
        $.ajax({
          type: "POST",
          url: "http://localhost/hotel/process/detele-listings.php",
          data: { data },
          success: function (response) {
            Swal.fire("ลบสำเร็จ!", "ห้องพักของคุณถูกลบแล้ว", "success");
            setTimeout(function () {
              location.reload();
            }, 2000);
          },
          error: function (error) {
            console.error(error);
          },
        });
      } catch (error) {
        console.error(error);
      }
    }
  });
}

$(document).ready(async function () {
  await fetchListings();
  if (localStorage.getItem("role") != "admin") {
    window.location.href = "/hotel";
  }
});
