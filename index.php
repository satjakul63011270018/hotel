<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HongSabye | Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/style.css">
</head>
<body>
    <?php include 'navbar.php';?>

    <section class="section-hero">
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <div class="col-lg-6 col-md-12 col-sm-12 ps-5 pt-5 sm-position">
                    <div class="card border-0 card-search" data-aos="fade-down" data-aos-duration="1500">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-3">
                                        <input type="email" class="form-control border-0" id="keyword" placeholder="ค้นหาชื่อหอพัก คอนโด ห้องพัก">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="mb-3">
                                        <select class="form-select border-0" id="province" aria-label="Floating label select example">
                                            <option value="" selected>เลือกจังหวัด</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!-- <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="mb-3">
                                        <select class="form-select border-0" id="floatingSelect" aria-label="Floating label select example">
                                            <option selected>ราคา</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div> -->
                                <!-- <div class="col-lg-6 col-md-12 col-sm-12">
                                    <div class="mb-3">
                                        <select class="form-select border-0" id="floatingSelect" aria-label="Floating label select example">
                                            <option selected>จังหวัด</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                        <button class="btn-search btn text-white" id="btn-search"><i class="fas fa-search me-2"></i>ค้นหา</button>
                    </div>
                    
                    <div class="mt-5" data-aos="fade-right" data-aos-duration="1500">
                        <p class="text-hero-small">ลงประกาศห้องเช่าฟรี ไม่มีค่าใช้จ่าย</p>
                        <p class="m-0 p-0 text-hero fw-bold">หาห้องเช่าง่าย</p>
                        <p class="text-hero fw-bold">เจ้าของห้องก็สบายใจ</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 d-none d-sm-none d-md-none d-lg-flex" data-aos="fade-left" data-aos-duration="1500">
                    <img class="img-hero" src="assets/img/main-img.png" alt="">
                </div>
            </div>
        </div>
    </section>

    <div class="container mt-4 mb-5">
        
        <div class="card w-100 card-advert border-0" data-aos="fade-up" data-aos-anchor-placement="top-center">
            <div class="card-body">
                <div class="row align-items-center h-100">
                    <p class="text-center">พื้นที่โฆษณา</p>
                </div>
            </div>
        </div>
        <div class="card w-100 mt-3 card-advert border-0" data-aos="fade-up" data-aos-anchor-placement="top-center">
            <div class="card-body">
                <div class="row align-items-center h-100">
                    <p class="text-center">พื้นที่โฆษณา</p>
                </div>
            </div>
        </div>
    </div>

    <section class="room-nearby-provinces mb-5">
        <div class="container">
            <div class="row">
                <p class="text-nearby-provinces1 mb-0 pb-0 ml-2">จังหวัดใกล้เคียง</p>
                <p class="text-nearby-provinces2">กรุงเทพและปริมณฑล รวมถึงต่างจังหวัด</p>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 mb-3" data-aos="fade-right" data-aos-duration="1500">
                    <div class="lazyload" style="border-radius: 5px; width: 100%; height: 20vw; margin-right: 10px;"></div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12" data-aos="fade-left" data-aos-duration="1500">
                    <div class="lazyload" style="border-radius: 5px; width: 100%; height: 20vw; margin-right: 10px;"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="room-nearby-provinces mb-5">
        <div class="container">
            <div class="d-flex justify-content-between">
                <div class="row">
                    <p class="text-nearby-provinces1 mb-0 pb-0 ml-2">ห้องเช่า</p>
                    <p class="text-nearby-provinces2">หอพัก คอนโด ห้องพัก</p>
                </div>
                <a class="btn btn-primary btn-seeall me-2 mt-4" href="/hotel/allroom.php" role="button">ดูทั้งหมด</a>
            </div>
            <div class="row" id="allroom">
                <div class="col-ld-3 col-md-3 col-sm-12 mb-4">
                    <div class="hotel-items">
                        <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 30%; height: 20px;"></div>
                    </div>
                </div>
                <div class="col-ld-3 col-md-3 col-sm-12 mb-4">
                    <div class="hotel-items">
                        <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 30%; height: 20px;"></div>
                    </div>
                </div>
                <div class="col-ld-3 col-md-3 col-sm-12 mb-4">
                    <div class="hotel-items">
                        <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 30%; height: 20px;"></div>
                    </div>
                </div>
                <div class="col-ld-3 col-md-3 col-sm-12 mb-4">
                    <div class="hotel-items">
                        <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 30%; height: 20px;"></div>
                    </div>
                </div>
                <div class="col-ld-3 col-md-3 col-sm-12 mb-4">
                    <div class="hotel-items">
                        <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 30%; height: 20px;"></div>
                    </div>
                </div>
                <div class="col-ld-3 col-md-3 col-sm-12 mb-4">
                    <div class="hotel-items">
                        <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 30%; height: 20px;"></div>
                    </div>
                </div>
                <div class="col-ld-3 col-md-3 col-sm-12 mb-4">
                    <div class="hotel-items">
                        <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                        <div class="lazyload" style="width: 30%; height: 20px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="room-nearby-provinces mb-5">
        <div class="container">
            <div class="d-flex justify-content-between">
                <div class="row">
                    <p class="text-nearby-provinces1 mb-0 pb-0 ml-2">ประชาสัมพันธ์</p>
                    <p class="text-nearby-provinces2">ติดตามความเคลื่อนไหวของ HongSabye</p>
                </div>
                <a class="btn btn-primary btn-seeall me-2 mt-4" href="#" role="button">ดูทั้งหมด</a>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-6 col-sm-12 me-0 ps-0 mb-2" data-aos="fade-right" data-aos-duration="1500">
                    <div class="lazyload" style="border-radius: 5px; width: 100%; height: 20vw; margin-right: 10px;"></div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 ms-0 ps-0" data-aos="fade-left" data-aos-duration="1500">
                    <div class="lazyload" style="border-radius: 5px; width: 100%; height: 20vw; margin-right: 10px;"></div>
                </div>
                
                <div class="col-lg-4 col-md-6 col-sm-12 mt-2 ps-0" data-aos="fade-up" data-aos-duration="1500">
                    <div class="lazyload" style="border-radius: 5px; width: 100%; height: 20vw; margin-right: 10px;"></div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 mt-2 ps-0" data-aos="fade-up" data-aos-duration="1500">
                    <div class="lazyload" style="border-radius: 5px; width: 100%; height: 20vw; margin-right: 10px;"></div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 mt-2 ps-0" data-aos="fade-up" data-aos-duration="1500">
                    <div class="lazyload" style="border-radius: 5px; width: 100%; height: 20vw; margin-right: 10px;"></div>
                </div>
            </div>
        </div>
    </section>

    <?php include 'footer.php';?>
    
   <script src="assets/js/pages/home.js"></script>
   
</body>
</html>
