<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HongSabye | Contact</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Option 1: Include in HTML -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
    integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <link rel="stylesheet" href="assets/style.css">
</head>
<body>
    <?php include 'navbar.php';?>
    <section class="section-hero">
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <div class="col-sm-12 col-sm-12 col-lg-6 ps-5 pt-5 d-none d-sm-none d-md-none d-lg-flex" data-aos="fade-right" data-aos-duration="1500">
                    <img class="img-hero" src="assets/img/img-contact.png" alt="">
                </div>
                <div class="col-sm-12 col-sm-12 col-lg-6 sm-position-contact" data-aos="fade-left" data-aos-duration="1500">
                    <h3>ติดต่อสอบถามหรือแจ้งปัญหาการใช้งาน</h3>
                    <p>ติดต่อข่าวสารและโปรโมชั่นได้ก่อนใครที่</p>
                    <button type="button" class="btn btn-success pe-5 ps-5"><i class="fab fa-line me-2"></i> Line</button>
                    <button type="button" class="btn btn-primary pe-5 ps-5"><i class="fab fa-facebook-square me-2"></i> Facbook</button>

                    <h3 class="mt-5">ติดต่อลงโฆษณา</h3>
                    <p class="m-0"><i class="fas fa-phone-alt me-2"></i> Phone : 099-983-1234 (M)</p>
                    <p><i class="fas fa-envelope me-2"></i> Email : Make@gmail.com</p>
                </div>
            </div>
        </div>
    </section>


    <?php include 'footer.php';?>
</body>
</html>