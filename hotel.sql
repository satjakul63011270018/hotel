-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 12, 2023 at 02:14 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel`
--

-- --------------------------------------------------------

--
-- Table structure for table `accommodations`
--

CREATE TABLE `accommodations` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `accommodationName` varchar(255) DEFAULT NULL,
  `addressNumber` varchar(255) DEFAULT NULL,
  `addressRoad` varchar(255) DEFAULT NULL,
  `addressAlley` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `amphure` varchar(255) DEFAULT NULL,
  `tambon` varchar(255) DEFAULT NULL,
  `zipCode` varchar(10) DEFAULT NULL,
  `facilities` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`facilities`)),
  `roomTypeName` varchar(255) DEFAULT NULL,
  `roomTypeLayout` varchar(255) DEFAULT NULL,
  `roomTypeSize` varchar(50) DEFAULT NULL,
  `roomTypeMonthlyRental` varchar(50) DEFAULT NULL,
  `roomTypeDailyRental` varchar(50) DEFAULT NULL,
  `roomTypeStatus` varchar(50) DEFAULT NULL,
  `dataWaterbill` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`dataWaterbill`)),
  `electricitybill` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`electricitybill`)),
  `service` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`service`)),
  `guarantee` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`guarantee`)),
  `phonebill` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`phonebill`)),
  `Internet` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`Internet`)),
  `details` text DEFAULT NULL,
  `contactInformationName` varchar(255) DEFAULT NULL,
  `contactInformationTel` varchar(20) DEFAULT NULL,
  `contactInformationEmail` varchar(255) DEFAULT NULL,
  `contactInformationLineID` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `name`) VALUES
(1, 'เครื่องปรับอากาศ'),
(2, 'เฟอร์นิเจอร์-ตู้, เตียง'),
(3, 'เครื่องทำน้ำอุ่น'),
(4, 'พัดลม'),
(5, 'มี TV'),
(6, 'เย็น'),
(7, 'โทรศัพท์สายตรง'),
(8, 'อินเทอร์เน็ตไร้สาย (WIFI) ในห้อง'),
(9, 'เคเบิลทีวี / ดาวเทียม'),
(10, 'อนุญาตให้เลี้ยงสัตว์'),
(11, 'อนุญาตให้สูบบุหรี่ในห้องพัก'),
(12, 'ที่จอดรถ'),
(13, 'ที่จอดรถมอเตอร์ไซด์/จักรยาน'),
(14, 'ลิฟต์'),
(15, 'สระว่ายน้ำ\r\n'),
(16, 'โรงยิม / ฟิตเนส'),
(17, 'มีระบบรักษาความปลอดภัย (keycard)'),
(18, 'มีระบบรักษาความปลอดภัย (สแกนลายนิ้วมือ)'),
(19, 'กล้องวงจรปิด (CCTV)'),
(20, 'รปภ.'),
(21, 'ร้านขายอาหาร'),
(22, 'ร้านค้า สะดวกซื้อ'),
(23, 'ร้านซัก-รีด / มีบริการเครื่องซักผ้า'),
(24, 'ร้านทำผม-เสริมสวย'),
(25, 'ถานี ​charge รถไฟฟ้า');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(10) NOT NULL,
  `acc_id` int(10) NOT NULL,
  `path` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `role`) VALUES
(1, 'Admin', 'Admin', 'admin@gmail.com', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accommodations`
--
ALTER TABLE `accommodations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accommodations`
--
ALTER TABLE `accommodations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
