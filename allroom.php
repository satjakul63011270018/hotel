<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HongSabye | AllRoom</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Option 1: Include in HTML -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
    integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/style.css">
</head>
<body>
    <?php include 'navbar.php';?>
    

    <section>
        <div class="container mt-5">
            <div class="row">
                <h3 class="ms-3">All Room</h3>

                <div class="col-lg-3 col-md-12 col-sm-12">
                    <div class="card w-100 card-search-allroom border-0 mt-2 shadow-lg">
                        <div class="card-body">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">ค้นหา</label>
                                <input type="email" class="form-control border-0" id="keyword" placeholder="ค้นหาชื่อหอพัก คอนโด ห้องพัก">
                            </div>
                            <!-- <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">อำเภอ/เขต</label>
                                <select class="form-select border-0" id="floatingSelect" aria-label="Floating label select example">
                                    <option selected>อำเภอ/เขต</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div> -->
                            <div class="mb-4">
                                <label for="exampleFormControlInput1" class="form-label">จังหวัด</label>
                                <select class="form-select border-0" aria-label="Floating label select example" id="province" >
                                    <option value="" selected>เลือกจังหวัด</option>
                                </select>
                            </div>

                            <!-- <div class="mb-3 mt-5">
                                <label for="exampleFormControlInput1" class="form-label">ราคาต่ำสุด</label>
                                <input type="email" class="form-control border-0" id="exampleFormControlInput1" placeholder="0">
                            </div>
                            <div class="mb-3">
                                <label for="exampleFormControlInput1" class="form-label">ราคาสูงสุด</label>
                                <input type="email" class="form-control border-0" id="exampleFormControlInput1" placeholder="0">
                            </div> -->

                            <button type="button" class="btn btn-warning w-100 mt-4 text-white" id="btn-search"><i class="fas fa-search me-2"></i> ค้นหา</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-12 col-sm-12 mt-2 " data-aos="fade-left" data-aos-duration="1500">
                    <div class="row allroom-sm-pos" id="allroom">
                        <div class="col-md-12 col-sm-6 col-lg-4 mb-4">
                            <div class="hotel-items">
                                <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 30%; height: 20px;"></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-lg-4 mb-4">
                            <div class="hotel-items">
                                <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 30%; height: 20px;"></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-lg-4 mb-4">
                            <div class="hotel-items">
                                <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 30%; height: 20px;"></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-lg-4 mb-4">
                            <div class="hotel-items">
                                <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 30%; height: 20px;"></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-lg-4 mb-4">
                            <div class="hotel-items">
                                <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 30%; height: 20px;"></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-lg-4 mb-4">
                            <div class="hotel-items">
                                <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 30%; height: 20px;"></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-lg-4 mb-4">
                            <div class="hotel-items">
                                <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 30%; height: 20px;"></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-lg-4 mb-4">
                            <div class="hotel-items">
                                <div class="lazyload" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 100%; height: 20px;margin-bottom: 5px;"></div>
                                <div class="lazyload" style="width: 30%; height: 20px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?php include 'footer.php';?>
     
   <script src="assets/js/pages/allroom.js"></script>
</body>
</html>