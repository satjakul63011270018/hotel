async function fetchListings() {
  const $roomMembers = $("#room-members");
  $roomMembers.empty();

  try {
    let data = localStorage.getItem("user_data");
    const response = await $.ajax({
      type: "POST",
      url: "http://localhost/hotel/process/get-show-room.php",
      data: { data },
    });

    response.forEach((item) => {
      const roomName = item.accommodationName || "ยังระบุชื่อห้อง";
      const monthlyRental = item.roomTypeMonthlyRental || "ยังระบุราคาห้อง";
      const location = `${item.province || "ยังไม่ระบุจังหวัด"} , ${
        item.amphure || "ยังไม่ระบุอำเภอ"
      }`;

      const imagesHTML = item.images
        .map((image, index) => {
          const isActive = index === 0 ? "active" : "";
          return `
            <div class="carousel-item h-100 ${isActive}">
              <a href="/hotel/roomdetail.php?id=${item.id}">
                <img src="../process/${image.path}" class="d-block w-100 h-100 img-room-slide" alt="../process/${image.path}" style=" object-fit: cover; object-position: top;"/>
              </a>
            </div>`;
        })
        .join("");

      const listingHTML = `
        <div class="col-md-12 col-sm-6 col-lg-3 mb-4">
          <div class="hotel-items">
            <div class="position-relative" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;">
              <div id="carouselExampleControls${item.id}" class="carousel slide h-100" data-bs-ride="carousel">
                <div class="carousel-inner h-100">
                  ${imagesHTML}
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls${item.id}" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls${item.id}" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
              </div>
            </div>
            <div class="position-relative room-title" style="margin-bottom: 5px">
              <h6 class="position-absolute">${roomName}</h6>
            </div>
            <div class="position-relative" style="width: 100%; height: 20px; margin-bottom: 5px">
              <p class="position-absolute">${monthlyRental}</p>
            </div>
            <div class="position-relative" style="width: 70%; height: 20px">
              <small class="position-absolute">${location}</small>
            </div>
            <div class="d-flex mt-2">
              <a href="/hotel/members/listings.php?room=${item.id}&type=edit" type="button" class="btn btn-warning text-white me-3">
                แก้ไขห้องพัก
              </a>
              <button type="button" class="btn btn-danger text-white" onclick="deteleListings(${item.id})">
                ลบห้องพัก
              </button>
            </div>
          </div>
        </div>`;

      $roomMembers.append(listingHTML);
    });
  } catch (error) {
    console.error(error);
  }
}

async function deteleListings(data) {
  Swal.fire({
    title: "คุณแน่ใจไหม?",
    text: "คุณจะเปลี่ยนกลับไม่ได้!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "ยืนยะน",
  }).then((result) => {
    if (result.isConfirmed) {
      try {
        $.ajax({
          type: "POST",
          url: "http://localhost/hotel/process/detele-listings.php",
          data: { data },
          success: function (response) {
            Swal.fire("ลบสำเร็จ!", "ห้องพักของคุณถูกลบแล้ว", "success");
            setTimeout(function () {
              location.reload();
            }, 2000);
          },
          error: function (error) {
            console.error(error);
          },
        });
      } catch (error) {
        console.error(error);
      }
    }
  });
}

$(document).ready(async function () {
  await fetchListings();
  let user_data = localStorage.getItem("user_data");
  if (user_data.length == 0) {
    window.location.href = "/hotel";
  }
});
