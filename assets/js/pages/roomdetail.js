async function fetchlistings() {
  const searchParams = new URLSearchParams(window.location.search);
  const room = searchParams.get("id");
  const responseFacilities = await Api.get(
    "http://localhost/hotel/assets/api/get-facilities.php"
  );
  let data = room;
  $.ajax({
    type: "POST",
    url: "http://localhost/hotel/process/get-listings.php",
    data: { data },
    success: function (response) {
      let res = response[0];
      let dataWaterbill = JSON.parse(res.dataWaterbill)[0];
      let electricitybill = JSON.parse(res.electricitybill)[0];
      let service = JSON.parse(res.service)[0];
      let guarantee = JSON.parse(res.guarantee)[0];
      let phonebill = JSON.parse(res.phonebill)[0];
      let Internet = JSON.parse(res.Internet)[0];
      let facilities = JSON.parse(res.facilities);

      $("#carousel-item").empty();
      const imagesHTML = res.images
        .map((image, index) => {
          const isActive = index === 0 ? "active" : "";
          return `
              <div class="carousel-item h-100 ${isActive}">
                <div class="img-box">
                  <img src="process/${image.path}" class="d-block w-100 h-100 img-room-slide" alt="process/${image.path}" style=" object-fit: cover; object-position: top;"/>
                </div>
              </div>`;
        })
        .join("");

      $("#carousel-item").append(imagesHTML);
      for (const item of facilities) {
        $("#facilities").append(
          `<div class="col-sm-6 col-md-6 col-lg-4 mb-4"><i class="fas fa-check-circle me-2"></i>${responseFacilities[item].name}</div>`
        );
      }

      if (dataWaterbill.id == "1") {
        $("#dataWaterbill").text(
          `${dataWaterbill.type} ${dataWaterbill.amount} บาท/หน่วย`
        );
      } else if (dataWaterbill.id == "2") {
        $("#dataWaterbill").text(`${dataWaterbill.type}`);
      } else if (dataWaterbill.id == "3") {
        $("#dataWaterbill").text(
          `${dataWaterbill.type} ${dataWaterbill.amount} บาท`
        );
      } else if (dataWaterbill.id == "4") {
        $("#dataWaterbill").text(
          `${dataWaterbill.type} ${dataWaterbill.amount} บาท/เดือน`
        );
      } else if (dataWaterbill.id == "5") {
        $("#dataWaterbill").text(`${dataWaterbill.type}`);
      } else if (dataWaterbill.id == "6") {
        $("#dataWaterbill").text(`${dataWaterbill.type}`);
      }

      if (electricitybill.id == "1") {
        $("#electricitybill").text(
          `${electricitybill.type} ${electricitybill.amount} บาท/หน่วย`
        );
      } else if (electricitybill.id == "2") {
        $("#electricitybill").text(`${electricitybill.type}`);
      } else if (electricitybill.id == "3") {
        $("#electricitybill").text(`${electricitybill.type}`);
      } else if (electricitybill.id == "4") {
        $("#electricitybill").text(`${electricitybill.type}`);
      }

      if (service.id == "1") {
        $("#service").text(`${service.amount} เดือน`);
      } else if (service.id == "2") {
        $("#service2").prop("checked", true);
      } else if (service.id == "3") {
        $("#service").text(`${service.type}`);
      }

      if (guarantee.id == "1") {
        $("#guarantee").text(`${guarantee.amount} เดือน`);
      } else if (guarantee.id == "2") {
        $("#guarantee").text(`${guarantee.amount} บาท`);
      } else if (guarantee.id == "3") {
        $("#guarantee").text(`${guarantee.type}`);
      } else if (guarantee.id == "5") {
        $("#guarantee").text(`${guarantee.type}`);
      }

      if (phonebill.id == "1") {
        $("#phonebill1").prop("checked", true);
        $("#phonebill-unit-1").val(phonebill.amount);
      } else if (phonebill.id == "2") {
        $("#phonebill2").prop("checked", true);
        $("#phonebill-unit-2").val(phonebill.amount);
      } else if (phonebill.id == "3") {
        $("#phonebill3").prop("checked", true);
      } else if (phonebill.id == "4") {
        $("#phonebill4").prop("checked", true);
      }

      if (Internet.id == "1") {
        $("#Internet1").prop("checked", true);
        $("#Internet-unit-1").val(Internet.amount);
      } else if (Internet.id == "2") {
        $("#Internet2").prop("checked", true);
      } else if (Internet.id == "3") {
        $("#Internet3").prop("checked", true);
      } else if (Internet.id == "4") {
        $("#Internet4").prop("checked", true);
      }

      $("#btn-see-allImg").attr("href", `process/${res.images[0].path}`);
      for (let index = 1; index < res.images.length; index++) {
        $("#fancybox-img").append(
          `<div class="col-sm-12 col-md-12 col-lg-12 mx-auto position-relative item"> <a href="process/${res.images[index].path}" data-fancybox="images"><p class="type-news-detail text-white pl-2 pr-2 ml-3 mt-3">ดูรูปภาพทั้งหมด</p><img class="img-lightbox" src="process/${res.images[index].path}" /></a></div>`
        );
      }

      $(".text-roomdetail-1").text(res.accommodationName);
      $(".text-roomdetail-2").text(`${res.amphure}, ${res.province}`);

      $(".roomTypeMonthlyRental").text(
        `${res.roomTypeMonthlyRental} บาท/เดือน`
      );

      $("#roomTypeSize").text(`${res.roomTypeSize} ตร.ม.`);

      $("#details").text(res.details);
      $("#contactInformationEmail").text(res.contactInformationEmail);
      $("#contactInformationLineID").text(res.contactInformationLineID);
      $("#contactInformationName").text(res.contactInformationName);
      $("#contactInformationTel").text(res.contactInformationTel);
    },
    error: function (error) {
      console.error(error);
    },
  });
}

$(document).ready(async function () {
  await fetchlistings();
});
