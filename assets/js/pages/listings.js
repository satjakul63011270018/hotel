const searchParams = new URLSearchParams(window.location.search);
const type = searchParams.get("type");
const room = searchParams.get("room");
const accommodationName = $("#accommodationName");
const addressNumber = $("#addressNumber");
const addressRoad = $("#addressRoad");
const addressAlley = $("#addressAlley");
const province = $("#province");
const amphure = $("#amphure");
const tambon = $("#tambon");
const zipCode = $("#zipCode");
const facilities = $("#facilities");
const roomTypeName = $("#roomTypeName");
const roomTypeLayout = $("#roomTypeLayout");
const roomTypeSize = $("#roomTypeSize");
const roomTypeMonthlyRental = $("#roomTypeMonthlyRental");
const roomTypeDailyRental = $("#roomTypeDailyRental");
const roomTypeStatus = $("#roomTypeStatus");
const details = $("#details");
const photo = $("#photo");
const contactInformationName = $("#contactInformationName");
const contactInformationTel = $("#contactInformationTel");
const contactInformationEmail = $("#contactInformationEmail");
const contactInformationLineID = $("#contactInformationLineID");
const btnSave = $(".btn-save");

let FacilitiesData = [];

async function fetchFacilitiesData() {
  try {
    const response = await Api.get(
      "http://localhost/hotel/assets/api/get-facilities.php"
    );
    FacilitiesData = response;
    for (const item of response) {
      $("#facilities").append(`
        <div class="col-4">
          <div class="form-check mb-2 mt-2">
            <input
              class="form-check-input"
              type="checkbox"
              value=""
              id="Checkfacilities${item.id}"
              data-box="${item.id}"
            />
            <label class="form-check-label" for="flexCheckDefault">
              ${item.name}
            </label>
          </div>
        </div>
      `);
    }
  } catch (error) {
    console.error(error);
  }
}

async function fetchFacilitiesDataProvince() {
  try {
    const response = await fetch(
      "https://raw.githubusercontent.com/kongvut/thai-province-data/master/api_province_with_amphure_tambon.json"
    );
    const data = await response.json();

    const provinceSelect = $("#province");
    const amphureSelect = $("#amphure");
    const tambonSelect = $("#tambon");
    const zipCodeInput = $("#zipCode");

    provinceSelect.on("change", function () {
      const selectedProvince = $(this).val();
      const selectedAmphure = data.find(
        (item) => item.name_th === selectedProvince
      ).amphure;

      // Clear the previous options in amphure and tambon selects
      amphureSelect.empty();
      tambonSelect.empty();

      for (const amphure of selectedAmphure) {
        const option = $("<option>").val(amphure.name_th).text(amphure.name_th);
        amphureSelect.append(option);
      }
    });

    amphureSelect.on("change", function () {
      const selectedAmphure = $(this).val();
      const selectedProvinceData = data.find((item) =>
        item.amphure.some((amphure) => amphure.name_th === selectedAmphure)
      );
      const selectedTambon = selectedProvinceData.amphure.find(
        (amphure) => amphure.name_th === selectedAmphure
      ).tambon;

      // Clear the previous options in the tambon select
      tambonSelect.empty();

      for (const tambon of selectedTambon) {
        const option = $("<option>").val(tambon.name_th).text(tambon.name_th);
        tambonSelect.append(option);
      }
    });

    // Populate the province select initially
    for (const item of data) {
      const option = $("<option>").val(item.name_th).text(item.name_th);
      provinceSelect.append(option);
    }

    tambonSelect.on("change", function () {
      const selectedTambon = $(this).val();
      const selectedProvinceData = data.find((item) =>
        item.amphure.some((amphure) => amphure.name_th === amphureSelect.val())
      );
      const selectedTambonData = selectedProvinceData.amphure
        .find((amphure) => amphure.name_th === amphureSelect.val())
        .tambon.find((tambon) => tambon.name_th === selectedTambon);

      zipCodeInput.val(selectedTambonData.zip_code);
    });
  } catch (error) {
    console.error(error);
  }
}

async function upLoadimg() {
  $("#photo").change(async function () {
    const files = this.files;

    if (files.length > 0) {
      $(".holder").empty();

      for (const file of files) {
        if (file.type.startsWith("image/")) {
          let reader = new FileReader();
          reader.onload = function (event) {
            // Create an image element for each preview
            let imgElement = $("<img>")
              .attr("src", event.target.result)
              .attr("alt", "pic");
            let cardDiv = $("<div>")
              .addClass(
                "card card-previews position-relative mt-3 p-0 ms-2 me-2"
              )
              .append(
                $("<div>")
                  .addClass("card-body p-0 m-0")
                  .append(imgElement)
                  .append(
                    $("<button>")
                      .addClass(
                        "btn btn-danger btn-del-img position-absolute top-0 end-0 me-2 mt-2"
                      )
                      .text("X")
                      .click(function () {
                        // Remove the parent cardDiv when delete button is clicked
                        $(this).closest(".card-previews").remove();
                      })
                  )
              );
            $(".holder").append(cardDiv);
          };
          reader.readAsDataURL(file);

          // Send image data to the server using AJAX
          const formData = new FormData();
          formData.append("photo[]", file);
        }
      }
    }
  });
}

async function fetchlistings() {
  province.addClass("d-none");
  amphure.addClass("d-none");
  tambon.addClass("d-none");
  $("#provinceEdit").removeClass("d-none");
  $("#amphureEdit").removeClass("d-none");
  $("#tambonEdit").removeClass("d-none");
  let data = room;
  $.ajax({
    type: "POST",
    url: "http://localhost/hotel/process/get-listings.php",
    data: { data },
    success: function (response) {
      let res = response[0];
      let dataWaterbill = JSON.parse(res.dataWaterbill)[0];
      let electricitybill = JSON.parse(res.electricitybill)[0];
      let service = JSON.parse(res.service)[0];
      let guarantee = JSON.parse(res.guarantee)[0];
      let phonebill = JSON.parse(res.phonebill)[0];
      let Internet = JSON.parse(res.Internet)[0];

      let facilities = JSON.parse(res.facilities);
      for (const item of facilities) {
        $(`#Checkfacilities${item}`).prop("checked", true);
      }
      // imgEdit

      for (const itemImg of res.images) {
        $(".imgEdit").append(`
        <div class="card card-previews position-relative mt-3 p-0 ms-2 me-2">
          <div class="card-body p-0 m-0">
            <img src="../process/${itemImg.path}" alt="../process/${itemImg.path}"><button class="btn btn-danger btn-del-img position-absolute top-0 end-0 me-2 mt-2" onclick="deteleListings(${itemImg.id})">X</button>
          </div>
        </div>`);
      }

      if (dataWaterbill.id == "1") {
        $("#waterbill1").prop("checked", true);
        $("#waterbill-unit-1").val(dataWaterbill.amount);
      } else if (dataWaterbill.id == "2") {
        $("#waterbill2").prop("checked", true);
      } else if (dataWaterbill.id == "3") {
        $("#waterbill3").prop("checked", true);
        $("#waterbill-unit-2").val(dataWaterbill.amount);
      } else if (dataWaterbill.id == "4") {
        $("#waterbill4").prop("checked", true);
        $("#waterbill-unit-3").val(dataWaterbill.amount);
      } else if (dataWaterbill.id == "5") {
        $("#waterbill5").prop("checked", true);
      } else if (dataWaterbill.id == "6") {
        $("#waterbill6").prop("checked", true);
      }

      if (electricitybill.id == "1") {
        $("#electricitybill").prop("checked", true);
        $("#electricitybill-unit-1").val(electricitybill.amount);
      } else if (electricitybill.id == "2") {
        $("#electricitybill2").prop("checked", true);
      } else if (electricitybill.id == "3") {
        $("#electricitybill3").prop("checked", true);
      } else if (electricitybill.id == "4") {
        $("#electricitybill4").prop("checked", true);
      }

      if (service.id == "1") {
        $("#service1").prop("checked", true);
        $("#service-unit-1").val(service.amount);
      } else if (service.id == "2") {
        $("#service2").prop("checked", true);
      } else if (service.id == "3") {
        $("#service3").prop("checked", true);
      }

      if (guarantee.id == "1") {
        $("#guarantee1").prop("checked", true);
        $("#guarantee-unit-1").val(guarantee.amount);
      } else if (guarantee.id == "2") {
        $("#guarantee2").prop("checked", true);
        $("#guarantee-unit-2").val(guarantee.amount);
      } else if (guarantee.id == "3") {
        $("#guarantee3").prop("checked", true);
      } else if (guarantee.id == "5") {
        $("#guarantee5").prop("checked", true);
      }

      if (phonebill.id == "1") {
        $("#phonebill1").prop("checked", true);
        $("#phonebill-unit-1").val(phonebill.amount);
      } else if (phonebill.id == "2") {
        $("#phonebill2").prop("checked", true);
        $("#phonebill-unit-2").val(phonebill.amount);
      } else if (phonebill.id == "3") {
        $("#phonebill3").prop("checked", true);
      } else if (phonebill.id == "4") {
        $("#phonebill4").prop("checked", true);
      }

      if (Internet.id == "1") {
        $("#Internet1").prop("checked", true);
        $("#Internet-unit-1").val(Internet.amount);
      } else if (Internet.id == "2") {
        $("#Internet2").prop("checked", true);
      } else if (Internet.id == "3") {
        $("#Internet3").prop("checked", true);
      } else if (Internet.id == "4") {
        $("#Internet4").prop("checked", true);
      }

      accommodationName.val(res.accommodationName);
      addressNumber.val(res.addressNumber);
      addressRoad.val(res.addressRoad);
      addressAlley.val(res.addressAlley);
      $("#provinceEdit").val(res.province);
      $("#amphureEdit").val(res.amphure);
      $("#tambonEdit").val(res.tambon);
      zipCode.val(res.zipCode);
      roomTypeName.val(res.roomTypeName);
      roomTypeLayout.val(res.roomTypeLayout);
      roomTypeSize.val(res.roomTypeSize);
      roomTypeMonthlyRental.val(res.roomTypeMonthlyRental);
      roomTypeDailyRental.val(res.roomTypeDailyRental);
      roomTypeStatus.val(res.roomTypeStatus);
      details.val(res.details);
      photo.val(res.photo);
      contactInformationName.val(res.contactInformationName);
      contactInformationTel.val(res.contactInformationTel);
      contactInformationEmail.val(res.contactInformationEmail);
      contactInformationLineID.val(res.contactInformationLineID);
    },
    error: function (error) {
      console.error(error);
    },
  });
}

$(document).ready(async function () {
  await fetchFacilitiesData();
  await fetchFacilitiesDataProvince();
  await upLoadimg();

  if (type == "edit") {
    await fetchlistings();
  }

  btnSave.on("click", async function () {
    let dataCheckbox = [];
    for (const iterator of FacilitiesData) {
      if ($(`#Checkfacilities${iterator.id}`).is(":checked")) {
        dataCheckbox.push(iterator.id);
      }
    }

    const dataWaterbill = [];
    const electricitybill = [];
    const service = [];
    const guarantee = [];
    const phonebill = [];
    const Internet = [];

    function addWaterbillEntry(id, type, amount) {
      dataWaterbill.push({ id, type, amount });
    }

    function addElectricitybill(id, type, amount) {
      electricitybill.push({ id, type, amount });
    }

    function addservice(id, type, amount) {
      service.push({ id, type, amount });
    }

    function addguarantee(id, type, amount) {
      guarantee.push({ id, type, amount });
    }

    function addphonebill(id, type, amount) {
      phonebill.push({ id, type, amount });
    }

    function addInternet(id, type, amount) {
      Internet.push({ id, type, amount });
    }

    if ($("#waterbill1").is(":checked")) {
      addWaterbillEntry("1", "ตามยูนิตที่ใช้", $("#waterbill-unit-1").val());
    }
    if ($("#waterbill2").is(":checked")) {
      addWaterbillEntry(
        "2",
        "ตามยูนิตที่ใช้ ราคาต่อยูนิตตามที่การประปากำหนด",
        null
      );
    }
    if ($("#waterbill3").is(":checked")) {
      addWaterbillEntry("3", "เหมาจ่ายต่อคน", $("#waterbill-unit-2").val());
    }
    if ($("#waterbill4").is(":checked")) {
      addWaterbillEntry("4", "เหมาจ่ายต่อห้อง", $("#waterbill-unit-3").val());
    }
    if ($("#waterbill5").is(":checked")) {
      addWaterbillEntry("5", "รวมในค่าห้องแล้ว", null);
    }
    if ($("#waterbill6").is(":checked")) {
      addWaterbillEntry("6", "โทรสอบถาม", null);
    }

    if ($("#electricitybill1").is(":checked")) {
      addElectricitybill(
        "1",
        "ตามยูนิตที่ใช้",
        $("#electricitybill-unit-1").val()
      );
    }
    if ($("#electricitybill2").is(":checked")) {
      addElectricitybill(
        "2",
        "ตามยูนิตที่ใช้ ราคาต่อยูนิตตามที่การค่าไฟกำหนด",
        null
      );
    }
    if ($("#electricitybill3").is(":checked")) {
      addElectricitybill("3", "รวมในค่าห้องแล้ว", null);
    }
    if ($("#electricitybill4").is(":checked")) {
      addElectricitybill("4", "โทรสอบถาม", null);
    }

    if ($("#service1").is(":checked")) {
      addservice("1", null, $("#service-uint-1").val());
    }
    if ($("#service2").is(":checked")) {
      addservice("2", "รวมในค่าห้องแล้ว", null);
    }
    if ($("#service3").is(":checked")) {
      addservice("3", "โทรสอบถาม", null);
    }

    if ($("#guarantee1").is(":checked")) {
      addguarantee("1", null, $("#guarantee-unit-1").val());
    }
    if ($("#guarantee2").is(":checked")) {
      addguarantee("2", null, $("#guarantee-unit-2").val());
    }
    if ($("#guarantee3").is(":checked")) {
      addguarantee("3", "ไม่มีการเก็บค่าเช่าล่วงหน้า", null);
    }
    if ($("#guarantee5").is(":checked")) {
      addguarantee("5", "โทรสอบถาม", null);
    }

    if ($("#phonebill1").is(":checked")) {
      addphonebill("1", "คิดเป็นรายนาที", $("#phonebill-unit-1").val());
    }
    if ($("#phonebill2").is(":checked")) {
      addphonebill("2", "คิดเป็นรายครั้ง", $("#phonebill-unit-2").val());
    }
    if ($("#phonebill3").is(":checked")) {
      addphonebill("3", "ไม่มีโทรศัพท์", null);
    }
    if ($("#phonebill4").is(":checked")) {
      addphonebill("4", "โทรสอบถาม", null);
    }

    if ($("#Internet1").is(":checked")) {
      addInternet("1", null, $("#Internet-unit-1").val());
    }
    if ($("#Internet2").is(":checked")) {
      addInternet("2", "รวมในค่าห้องแล้ว", null);
    }
    if ($("#Internet3").is(":checked")) {
      addInternet("3", "ไม่มีอินเทอร์เน็ต", null);
    }
    if ($("#Internet4").is(":checked")) {
      addInternet("4", "โทรสอบถาม", null);
    }

    let data = {};
    if (type == "edit") {
      data = {
        id: room,
        username: localStorage.getItem("user_data"),
        accommodationName: $("#accommodationName").val(),
        addressNumber: $("#addressNumber").val(),
        addressRoad: $("#addressRoad").val(),
        addressAlley: $("#addressAlley").val(),
        province: $("#province").val(),
        amphure: $("#amphure").val(),
        tambon: $("#tambon").val(),
        zipCode: $("#zipCode").val(),
        facilities: {
          isCheck: JSON.stringify(dataCheckbox),
        },
        roomTypeName: $("#roomTypeName").val(),
        roomTypeLayout: $("#roomTypeLayout").val(),
        roomTypeSize: $("#roomTypeSize").val(),
        roomTypeMonthlyRental: $("#roomTypeMonthlyRental").val(),
        roomTypeDailyRental: $("#roomTypeDailyRental").val(),
        roomTypeStatus: $("#roomTypeStatus").val(),
        dataWaterbill: JSON.stringify(dataWaterbill),
        electricitybill: JSON.stringify(electricitybill),
        service: JSON.stringify(service),
        guarantee: JSON.stringify(guarantee),
        phonebill: JSON.stringify(phonebill),
        Internet: JSON.stringify(Internet),
        details: $("#details").val(),
        contactInformationName: $("#contactInformationName").val(),
        contactInformationTel: $("#contactInformationTel").val(),
        contactInformationEmail: $("#contactInformationEmail").val(),
        contactInformationLineID: $("#contactInformationLineID").val(),
      };
    } else {
      data = {
        username: localStorage.getItem("user_data"),
        accommodationName: $("#accommodationName").val(),
        addressNumber: $("#addressNumber").val(),
        addressRoad: $("#addressRoad").val(),
        addressAlley: $("#addressAlley").val(),
        province: $("#province").val(),
        amphure: $("#amphure").val(),
        tambon: $("#tambon").val(),
        zipCode: $("#zipCode").val(),
        facilities: {
          isCheck: JSON.stringify(dataCheckbox),
        },
        roomTypeName: $("#roomTypeName").val(),
        roomTypeLayout: $("#roomTypeLayout").val(),
        roomTypeSize: $("#roomTypeSize").val(),
        roomTypeMonthlyRental: $("#roomTypeMonthlyRental").val(),
        roomTypeDailyRental: $("#roomTypeDailyRental").val(),
        roomTypeStatus: $("#roomTypeStatus").val(),
        dataWaterbill: JSON.stringify(dataWaterbill),
        electricitybill: JSON.stringify(electricitybill),
        service: JSON.stringify(service),
        guarantee: JSON.stringify(guarantee),
        phonebill: JSON.stringify(phonebill),
        Internet: JSON.stringify(Internet),
        details: $("#details").val(),
        contactInformationName: $("#contactInformationName").val(),
        contactInformationTel: $("#contactInformationTel").val(),
        contactInformationEmail: $("#contactInformationEmail").val(),
        contactInformationLineID: $("#contactInformationLineID").val(),
      };
    }

    let url = "";
    if (type == "edit") {
      url = "http://localhost/hotel/process/update-listings.php";
    } else {
      url = "http://localhost/hotel/process/create-listings.php";
    }
    $.ajax({
      type: "POST",
      url: url,
      data: { data },
      success: function (response) {
        if ($("#photo").val().length > 0) {
          let formData = new FormData();
          for (let i = 0; i < $("#photo").prop("files").length; i++) {
            formData.append("photos[]", $("#photo").prop("files")[i]);
          }
          if (type == "edit") {
            formData.append("acc_id", response.id);
          } else {
            formData.append("acc_id", JSON.parse(response).id);
          }

          $.ajax({
            url: "http://localhost/hotel/process/upload.php",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            method: "POST",
            type: "POST",
            success: function (response) {
              // console.log("Upload successful:", response);
            },
            error: function (xhr, status, error) {
              console.error("Upload failed:", error);
            },
          });
        }
        Swal.fire("บันทึกสำเร็จ!", "", "success");
        setTimeout(function () {
          window.location.href = "/hotel/members";
        }, 2000);
      },
      error: function (error) {
        console.error(error);
      },
    });

    console.log(data);
  });
});

async function deteleListings(data) {
  Swal.fire({
    title: "คุณแน่ใจไหม?",
    text: "คุณจะเปลี่ยนกลับไม่ได้!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "ยืนยะน",
  }).then((result) => {
    if (result.isConfirmed) {
      try {
        $.ajax({
          type: "POST",
          url: "http://localhost/hotel/process/detele-listings.php",
          data: { data },
          success: function (response) {
            Swal.fire("ลบสำเร็จ!", "ห้องพักของคุณถูกลบแล้ว", "success");
            setTimeout(function () {
              location.reload();
            }, 2000);
          },
          error: function (error) {
            console.error(error);
          },
        });
      } catch (error) {
        console.error(error);
      }
    }
  });
}
