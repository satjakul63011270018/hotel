async function fetchFacilitiesDataProvince() {
  try {
    const response = await fetch(
      "https://raw.githubusercontent.com/kongvut/thai-province-data/master/api_province_with_amphure_tambon.json"
    );
    const data = await response.json();

    const provinceSelect = $("#province");
    const amphureSelect = $("#amphure");
    const tambonSelect = $("#tambon");
    const zipCodeInput = $("#zipCode");

    provinceSelect.on("change", function () {
      const selectedProvince = $(this).val();
      const selectedAmphure = data.find(
        (item) => item.name_th === selectedProvince
      ).amphure;

      // Clear the previous options in amphure and tambon selects
      amphureSelect.empty();
      tambonSelect.empty();

      for (const amphure of selectedAmphure) {
        const option = $("<option>").val(amphure.name_th).text(amphure.name_th);
        amphureSelect.append(option);
      }
    });

    amphureSelect.on("change", function () {
      const selectedAmphure = $(this).val();
      const selectedProvinceData = data.find((item) =>
        item.amphure.some((amphure) => amphure.name_th === selectedAmphure)
      );
      const selectedTambon = selectedProvinceData.amphure.find(
        (amphure) => amphure.name_th === selectedAmphure
      ).tambon;

      // Clear the previous options in the tambon select
      tambonSelect.empty();

      for (const tambon of selectedTambon) {
        const option = $("<option>").val(tambon.name_th).text(tambon.name_th);
        tambonSelect.append(option);
      }
    });

    // Populate the province select initially
    for (const item of data) {
      const option = $("<option>").val(item.name_th).text(item.name_th);
      provinceSelect.append(option);
    }

    tambonSelect.on("change", function () {
      const selectedTambon = $(this).val();
      const selectedProvinceData = data.find((item) =>
        item.amphure.some((amphure) => amphure.name_th === amphureSelect.val())
      );
      const selectedTambonData = selectedProvinceData.amphure
        .find((amphure) => amphure.name_th === amphureSelect.val())
        .tambon.find((tambon) => tambon.name_th === selectedTambon);

      zipCodeInput.val(selectedTambonData.zip_code);
    });
  } catch (error) {
    console.error(error);
  }
}

async function fetchListings() {
  const $roomMembers = $("#allroom");
  $roomMembers.empty();

  try {
    // let data = localStorage.getItem("user_data");
    const response = await $.ajax({
      type: "GET",
      url: "http://localhost/hotel/process/get-show-room-limit.php",
    });

    let res = JSON.parse(response);

    res.forEach((item) => {
      const roomName = item.accommodationName || "ยังระบุชื่อห้อง";
      const monthlyRental = item.roomTypeMonthlyRental || "ยังระบุราคาห้อง";
      const location = `${item.province || "ยังไม่ระบุจังหวัด"} , ${
        item.amphure || "ยังไม่ระบุอำเภอ"
      }`;

      const imagesHTML = item.images
        .map((image, index) => {
          const isActive = index === 0 ? "active" : "";
          return `
            <div class="carousel-item h-100 ${isActive}">
              <a href="roomdetail.php?id=${item.id}">
                <img src="process/${image.path}" class="d-block w-100 h-100 img-room-slide" alt="process/${image.path}" style=" object-fit: cover; object-position: top;"/>
              </a>
            </div>`;
        })
        .join("");

      const listingHTML = `
        <div class="col-md-12 col-sm-6 col-lg-3 mb-4">
          <div class="hotel-items">
            <div class="position-relative" style="width: 100%; height: 240px; border-radius: 10px; margin-bottom: 10px;">
              <div id="carouselExampleControls${item.id}" class="carousel slide h-100" data-bs-ride="carousel">
                <div class="carousel-inner h-100">
                  ${imagesHTML}
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls${item.id}" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls${item.id}" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
              </div>
            </div>
            <div class="position-relative room-title" style="margin-bottom: 5px">
              <h6 class="position-absolute">${roomName}</h6>
            </div>
            <div class="position-relative" style="width: 100%; height: 20px; margin-bottom: 5px">
              <p class="position-absolute">${monthlyRental}</p>
            </div>
            <div class="position-relative" style="width: 70%; height: 20px">
              <small class="position-absolute">${location}</small>
            </div>
          </div>
        </div>`;

      $roomMembers.append(listingHTML);
    });
  } catch (error) {
    console.error(error);
  }
}

$(document).ready(async function () {
  await fetchListings();
  await fetchFacilitiesDataProvince();

  $("#btn-search").on("click", function () {
    const keyword = $("#keyword").val();
    const province = $("#province").val();
    window.location.href = `/hotel/allroom.php?keyword=${keyword}&province=${province}`;
  });
});
