<?php
// Include the connect.php file to establish the database connection
require_once '../../connect/connect.php';

function getFacilities() {
    try {
        // Get the PDO object from the connect.php file
        $pdo = require '../../connect/connect.php';

        // Prepare and execute the SQL query to select facilities data from the database
        $stmt = $pdo->prepare("SELECT * FROM facilities");
        $stmt->execute();

        // Fetch all rows as an associative array
        $facilities = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Convert the result to JSON
        $jsonResult = json_encode($facilities);

        return $jsonResult;
    } catch (PDOException $e) {
        // Handle any errors that occur during the query execution
        die("Error fetching facilities data: " . $e->getMessage());
    }
}

// Call the function to get the JSON data
$jsonData = getFacilities();

// Output the JSON data
header('Content-Type: application/json');
echo $jsonData;
?>
