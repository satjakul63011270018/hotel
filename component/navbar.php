<nav class="navbar-top d-none d-sm-none d-md-none d-lg-flex">
      <div class="container d-flex justify-content-between">
        <a href="/hotel">
          <img
            src="../assets/img/logo/logo_home2.png"
            class="img-logo ps-4"
            alt=""
          />
        </a>

        <ul class="nav-menu m-0 mt-1">
          <li>
            <a href="/hotel/" class="menu-link pt-3 text-center">หน้าแรก</a>
          </li>
          <li>
            <a href="/hotel/allroom.php" class="menu-link text-dark">ห้องพัก</a>
          </li>
          <li>
            <a href="/hotel/contact.php" class="menu-link active-cl text-dark"
              >ติดต่อเรา</a
            >
          </li>
        </ul>

        <div class="d-flex">
          <a
            href="javascript:void(0)"
            class="becomehost mt-1 text-center mt-auto me-3 pt-1 fw-bold"
            type="button"
            onclick="goToMember()"
          >
            ประกาศห้องพักฟรี
          </a>
          <a
            href="javascript:void(0)"
            class="btn btn-signin text-center mt-auto pt-2"
            data-bs-toggle="modal"
            data-bs-target="#exampleModal"
            
          >
            เข้าสู่ระบบ
          </a>
          <button class="btn btn-profile dropdown-toggle mt-auto d-none" type="button" id="profile" data-bs-toggle="dropdown" aria-expanded="false">
            Admin Admin
          </button>
          <ul class="dropdown-menu" aria-labelledby="profile">
            <li><a class="dropdown-item" href="#">setting</a></li>
            <li><a class="dropdown-item" onclick="logout()" href="#">ออกจากระบบ</a></li>
          </ul>
        </div>
      </div>
    </nav>
    <nav
      class="navbar navbar-expand-lg navbar-light d-sm-block d-lg-none w-100"
    >
      <div class="container-fluid">
        <a href="index.html">
          <img
            src="../assets/img/logo/logo_home2.png"
            class="img-logo ps-4"
            alt=""
          />
        </a>
        <button
          class="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-3 mt-3 mb-lg-0">
            <li class="nav-item">
              <a
                href="index.html"
                class="menu-link active-cl pt-3 text-center d-flex justify-content-center"
                >หน้าแรก</a
              >
            </li>
            <li class="nav-item">
              <a
                href="allroom.html"
                class="menu-link pt-3 pb-3 text-dark d-flex justify-content-center"
                >ห้องพัก</a
              >
            </li>
            <li class="nav-item">
              <a
                href="contact.html"
                class="menu-link text-dark d-flex justify-content-center"
                >ติดต่อเรา</a
              >
            </li>
          </ul>
          <div class="d-flex justify-content-center justify-items-center">
            <button
              href="javascript:void(0)"
              class="becomehost mt-1 text-center mt-auto me-3 pt-1 fw-bold"
              type="button"
              onclick="goToMember(e)"
            >
              ประกาศห้องพักฟรี
            </button>
            <a
              href="javascript:void(0)"
              class="btn btn-signin text-center mt-auto pt-2"
              data-bs-toggle="modal"
              data-bs-target="#exampleModal"
            >
              เข้าสู่ระบบ
            </a>
            <button class="btn btn-profile dropdown-toggle mt-auto d-none" type="button" id="profile" data-bs-toggle="dropdown" aria-expanded="false">
              Admin Admin
            </button>
            <ul class="dropdown-menu" aria-labelledby="profile">
              <li><a class="dropdown-item" href="#">setting</a></li>
              <li><a class="dropdown-item" onclick="logout()" href="#">ออกจากระบบ</a></li>
            </ul>
          </div>
        </div>
      </div>
    </nav>

    <script>
      function goToMember() {
        let user_data = localStorage.getItem("user_data");
        if (user_data.length > 0) {
          window.location.href = "/hotel/members";
        } else {
          $("#exampleModal").modal("show");
        }
      }
    </script>