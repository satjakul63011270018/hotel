<footer class="footer">
      <div class="container">
        <div class="row">
          <div
            class="col-lg-4 col-md-12 col-sm-12 mt-4 ps-5 footer-pos-sm-logo"
          >
            <a href="index.html">
              <img
                src="../assets/img/logo/logo_home2.png"
                class="img-logo ps-4"
                alt=""
              />
            </a>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12 mt-4 footer-pos-sm">
            <div class="col">
              <h5>เกี่ยวกับ</h5>
              <div class="row">
                <a class="text-secondary" href="">ห้องพักทั้งหมด</a>
                <a class="text-secondary" href="">ลงประกาศห้องเช่าฟรี</a>
                <a class="text-secondary" href="">สนใจลงโฆษณา</a>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12 mt-4 mb-4 footer-pos-sm">
            <div class="col">
              <h5>ติดต่อเรา</h5>
              <a class="text-secondary" href=""
                ><i class="fas fa-envelope me-2"></i> Make@gmail.com</a
              >
            </div>
            <div class="col mt-4">
              <h5>สนใจลงโฆษณา</h5>
              <a class="text-secondary" href=""
                ><i class="fas fa-phone-alt me-2"></i> 083-9859-8321 (M)</a
              >
            </div>
          </div>
        </div>
      </div>
    </footer>
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
          <div class="modal-body modal-login">
            <div class="row align-items-center h-100">
              <div class="col-sm-12 col-md-12 col-lg-6 d-none d-sm-none d-md-none d-lg-flex">
                <div class="bg-login align-items-center position-relative">
                  <img
                    src="../assets/img/login/login-img-01.png"
                    class="img-login center"
                    alt=""
                  />
                </div>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-6 modal-pos-sm">
                <div class="form-login">
                  <h5 class="text-center mb-4 me-3">เข้าสู่ระบบ</h5>
                  <div class="mb-3 me-3">
                    <input type="text" class="form-control" id="emailLogin" placeholder="Usename or Email"/>
                  </div>
                  <div class="mb-3 me-3">
                    <input type="password" class="form-control" id="passwordlLogin" placeholder="Password" />
                  </div>
                  <div class="me-3">
                    <button type="button" id="btn-login" class="btn btn-warning mt-3 w-100">
                      เข้าสู่ระบบ
                    </button>
                  </div>
                </div>
                <div class="form-register d-none">
                  <h5 class="text-center mb-4 me-3">สมัครสมาชิค</h5>
                  <div class="mb-3 me-3">
                    <input type="text" class="form-control" id="first_name" placeholder="ชื่อ"/>
                  </div>
                  <div class="mb-3 me-3">
                    <input type="text" class="form-control"id="last_name" placeholder="นามสกุล" />
                  </div>
                  <div class="mb-3 me-3">
                    <input type="text" class="form-control" id="email" placeholder="Usename or Email"/>
                  </div>
                  <div class="mb-3 me-3">
                    <input type="password" class="form-control" id="password" placeholder="Password"/>
                  </div>
                
                  <div class="me-3">
                    <button type="button" id="btn-register" class="btn btn-warning mt-3 w-100">
                      สมัครสมาชิก
                    </button>
                  </div>

                </div>

                <div
                  class="d-flex align-items-center justify-content-center mt-5 me-3"
                >
                  <p style="width: 100%; height: 1px; background: #adadad"></p>
                  <p style="margin: 0 20px; margin-top: -13px">หรือ</p>
                  <p style="width: 100%; height: 1px; background: #adadad"></p>
                </div>

                <h6 class="text-center mt-4 me-3 form-login">
                  ยังไม่มีบัญชีผู้ใช้ ?
                  <span class="text-warning" style="cursor: pointer" id="register">สมัครสมาชิก</span>
                </h6>
                <h6 class="text-center mt-4 me-3 form-register d-none">
                  ยังไม่มีบัญชีผู้ใช้ ?
                  <span class="text-warning" style="cursor: pointer" id="login">เข้าสู่ระบบ</span
                  >
                </h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../assets/js/connect/connect.js"></script>
    <script src="../assets/js/pages/register.js"></script>
    <script src="../assets/js/pages/login.js"></script>
    <script src="../assets/utils.js"></script>

    <script>
      AOS.init();
      $( "#register" ).on( "click", function() {
          $('.form-login').addClass('d-none');
          $('.form-register').removeClass('d-none');
      } );
      $( "#login" ).on( "click", function() {
          $('.form-register').addClass('d-none');
          $('.form-login').removeClass('d-none');
      } );
    </script>