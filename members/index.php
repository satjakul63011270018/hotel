<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>HongSabye | AllRoom</title>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <!-- Option 1: Include in HTML -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
      integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
    <link rel="stylesheet" href="../assets/style.css" />
  </head>
  <body>
    <?php include '../component/navbar.php';?>
    <section>
      <div class="container mt-5">
        <div class="row">
          <div class="card border-0 shadow-lg mt-5">
            <div class="card-body">
             <div class="d-flex justify-content-between align-items-center">
                <div><h3 class="ms-3 mt-3 mb-5">ประกาศของคุณ</h3></div>
                <div></div>
                <div><a class="btn btn-primary" href="/hotel/members/listings.php" role="button">ลงประกาศ</a></div>
             </div>
              
              <div
                class="col-lg-12 col-md-12 col-sm-12 mt-2"
              >
                <div class="row allroom-sm-pos" id="room-members">
                  <div class="col-md-12 col-sm-6 col-lg-3 mb-4">
                    <div class="hotel-items">
                      <div
                        class="lazyload"
                        style="
                          width: 100%;
                          height: 240px;
                          border-radius: 10px;
                          margin-bottom: 10px;
                        "
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 100%; height: 20px; margin-bottom: 5px"
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 100%; height: 20px; margin-bottom: 5px"
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 30%; height: 20px"
                      ></div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-6 col-lg-3 mb-4">
                    <div class="hotel-items">
                      <div
                        class="lazyload"
                        style="
                          width: 100%;
                          height: 240px;
                          border-radius: 10px;
                          margin-bottom: 10px;
                        "
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 100%; height: 20px; margin-bottom: 5px"
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 100%; height: 20px; margin-bottom: 5px"
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 30%; height: 20px"
                      ></div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-6 col-lg-3 mb-4">
                    <div class="hotel-items">
                      <div
                        class="lazyload"
                        style="
                          width: 100%;
                          height: 240px;
                          border-radius: 10px;
                          margin-bottom: 10px;
                        "
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 100%; height: 20px; margin-bottom: 5px"
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 100%; height: 20px; margin-bottom: 5px"
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 30%; height: 20px"
                      ></div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-6 col-lg-3 mb-4">
                    <div class="hotel-items">
                      <div
                        class="lazyload"
                        style="
                          width: 100%;
                          height: 240px;
                          border-radius: 10px;
                          margin-bottom: 10px;
                        "
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 100%; height: 20px; margin-bottom: 5px"
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 100%; height: 20px; margin-bottom: 5px"
                      ></div>
                      <div
                        class="lazyload"
                        style="width: 30%; height: 20px"
                      ></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include '../component/footer.php';?>

    <script src="../assets/js/pages/members.js"></script>
  </body>
</html>
