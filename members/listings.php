<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>HongSabye | Contact</title>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <!-- Option 1: Include in HTML -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
      integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />

    <link rel="stylesheet" href="../assets/style.css" />
  </head>
  <body>
    
  <?php include '../component/navbar.php';?>

    <section class="section-hero">
      <div class="container h-100">
        <div class="row align-items-center h-100">
          <div class="card shadow-lg border-0 mt-4">
            <div class="card-body">
              <div class="d-flex align-items-end">
                <button type="button" onclick="history.back()" class="btn btn-primary me-2"><i class="fas fa-backward"></i></button>
                <h2 class="mt-3"><b>ลงประกาศห้องพัก</b></h2>
              </div>
              <div class="row mt-4">
                <hr />
                <h5><b>1. ข้อมูลที่พัก</b></h5>
                <div class="col-6">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label"
                      >ชื่อที่พัก
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="accommodationName"
                      aria-describedby="emailHelp"
                    />
                  </div>
                </div>
              </div>
              <div class="row">
                <h5 class="mb-3 mt-2"><b>ที่อยู่</b></h5>
                <div class="col-4">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">
                      เลขที่
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="addressNumber"
                      aria-describedby="emailHelp"
                    />
                  </div>
                </div>
                <div class="col-4">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label"
                      >ถนน
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="addressRoad"
                      aria-describedby="emailHelp"
                    />
                  </div>
                </div>
                <div class="col-4">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label"
                      >ซอย
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="addressAlley"
                      aria-describedby="emailHelp"
                    />
                  </div>
                </div>
                <div class="col-4">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label"
                      >จังหวัด
                    </label>
                    <select
                      class="form-select"
                      aria-label="Default select example"
                      id="province"
                    >
                      <option selected>เลือกจังหวัด</option>
                    </select>
                    <input
                      type="text"
                      class="form-control d-none"
                      id="provinceEdit"
                    />
                  </div>
                </div>
                <div class="col-4">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label"
                      >อำเภอ/เขต
                    </label>
                    <select
                      class="form-select"
                      aria-label="Default select example"
                      id="amphure"
                    >
                      <option selected>เลือกอำเภอ/เขต</option>
                    </select>
                    <input
                      type="text"
                      class="form-control d-none"
                      id="amphureEdit"
                    />
                  </div>
                </div>
                <div class="col-4">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label"
                      >ตำบล/แขวง
                    </label>
                    <select
                      class="form-select"
                      aria-label="Default select example"
                      id="tambon"
                    >
                      <option selected>เลือกตำบล/แขวง</option>
                    </select>
                    <input
                      type="text"
                      class="form-control d-none"
                      id="tambonEdit"
                    />
                  </div>
                </div>
                <div class="col-4">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label"
                      >รหัสไปรษณีย์
                    </label>
                    <input
                      type="text"
                      class="form-control"
                      id="zipCode"
                      aria-describedby="emailHelp"
                    />
                  </div>
                </div>
              </div>
              <div class="row mt-4">
                <h5><b>2. สิ่งอำนวยความสะดวก</b></h5>
                <div class="row" id="facilities"></div>
              </div>
              <div class="row mt-4">
                <h5><b>3. ประเภทห้องพัก</b></h5>
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-3">
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label"
                            >ชื่อประเภทห้อง
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            id="roomTypeName"
                            aria-describedby="emailHelp"
                          />
                        </div>
                      </div>
                      <div class="col-3">
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label"
                            >รูปแบบห้อง
                          </label>
                          <select
                            class="form-select"
                            aria-label="Default select example"
                            id="roomTypeLayout"
                          >
                            <option selected>เลือกรูปแบบห้อง</option>
                            <option value="สตูดิโอ">สตูดิโอ</option>
                            <option value="1 ห้องนอน">1 ห้องนอน</option>
                            <option value="2 ห้องนอน">2 ห้องนอน</option>
                            <option value="3 ห้องนอน">3 ห้องนอน</option>
                            <option value="4 ห้องนอน">4 ห้องนอน</option>
                            <option value="5 ห้องนอน">5 ห้องนอน</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-3">
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label"
                            >ขนาดห้อง (ตร.ม.)
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            id="roomTypeSize"
                            aria-describedby="emailHelp"
                          />
                        </div>
                      </div>
                      <div class="col-3">
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label"
                            >เช่ารายเดือน (บาท/เดือน)
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            id="roomTypeMonthlyRental"
                            aria-describedby="emailHelp"
                          />
                        </div>
                      </div>
                      <div class="col-3">
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label"
                            >เช่ารายวัน (บาท/วัน)
                          </label>
                          <input
                            type="text"
                            class="form-control"
                            id="roomTypeDailyRental"
                            aria-describedby="emailHelp"
                          />
                        </div>
                      </div>
                      <div class="col-3">
                        <div class="mb-3">
                          <label for="exampleInputEmail1" class="form-label"
                            >สถานะห้อง
                          </label>
                          <select
                            class="form-select"
                            aria-label="Default select example"
                            id="roomTypeStatus"
                          >
                            <option selected>เลือกสถานะห้อง</option>
                            <option value="เปิด">เปิด</option>
                            <option value="ปิด">ปิด</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row mt-4">
                <h5><b>4. ค่าใช้จ่าย</b></h5>
                <div class="col-2 mt-4">
                  <p>ค่าน้ำ</p>
                </div>
                <div class="col-10">
                  <div class="form-check mb-3 mt-3">
                    <div class="d-flex align-items-center" style="height: 36px">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="waterbill"
                        id="waterbill1"
                        checked
                      />
                      <label
                        class="form-check-label ms-2 me-2"
                        for="waterbill1"
                      >
                        ตามยูนิตที่ใช้
                      </label>
                      <div class="input-group">
                        <input
                          type="text"
                          class="form-control form-unit"
                          aria-label="Recipient's username"
                          aria-describedby="basic-addon2"
                          id="waterbill-unit-1"
                        />
                        <span
                          class="input-group-text form-unit-text"
                          id="basic-addon2"
                          >บาทต่อยูนิต</span
                        >
                      </div>
                    </div>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="waterbill"
                      id="waterbill2"
                    />
                    <label class="form-check-label" for="waterbill2">
                      ตามยูนิตที่ใช้ ราคาต่อยูนิตตามที่การประปากำหนด
                    </label>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <div class="d-flex align-items-center" style="height: 36px">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="waterbill"
                        id="waterbill3"
                      />
                      <label
                        class="form-check-label ms-2 me-2"
                        for="waterbill3"
                      >
                        เหมาจ่ายต่อคน
                      </label>
                      <div class="input-group">
                        <input
                          type="text"
                          class="form-control form-unit"
                          aria-label="Recipient's username"
                          aria-describedby="basic-addon2"
                          id="waterbill-unit-2"
                        />
                        <span
                          class="input-group-text form-unit-text"
                          id="basic-addon2"
                          >บาท/เดือน</span
                        >
                      </div>
                    </div>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <div class="d-flex align-items-center" style="height: 36px">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="waterbill"
                        id="waterbill4"
                      />
                      <label
                        class="form-check-label ms-2 me-2"
                        for="waterbill4"
                      >
                        เหมาจ่ายต่อห้อง
                      </label>
                      <div class="input-group">
                        <input
                          type="text"
                          class="form-control form-unit"
                          aria-label="Recipient's username"
                          aria-describedby="basic-addon2"
                          id="waterbill-unit-3"
                        />
                        <span
                          class="input-group-text form-unit-text"
                          id="basic-addon2"
                          >บาทต่อห้อง/เดือน</span
                        >
                      </div>
                    </div>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="waterbill5"
                      name="waterbill"
                    />
                    <label class="form-check-label" for="waterbill5">
                      รวมในค่าห้องแล้ว
                    </label>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="waterbill6"
                      name="waterbill"
                    />
                    <label class="form-check-label" for="waterbill6">
                      โทรสอบถาม
                    </label>
                  </div>
                </div>
                <div class="col-2 mt-4">
                  <p>ค่าไฟ</p>
                </div>
                <div class="col-10">
                  <div class="form-check mb-3 mt-3">
                    <div class="d-flex align-items-center" style="height: 36px">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="electricitybill"
                        id="electricitybill1"
                        checked
                      />
                      <label
                        class="form-check-label ms-2 me-2"
                        for="electricitybill1"
                      >
                        ตามยูนิตที่ใช้
                      </label>
                      <div class="input-group">
                        <input
                          type="text"
                          class="form-control form-unit"
                          aria-label="Recipient's username"
                          aria-describedby="basic-addon2"
                          id="electricitybill-unit-1"
                        />
                        <span
                          class="input-group-text form-unit-text"
                          id="basic-addon2"
                          >บาทต่อยูนิต</span
                        >
                      </div>
                    </div>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="electricitybill"
                      id="electricitybill2"
                    />
                    <label class="form-check-label" for="electricitybill2">
                      ตามยูนิตที่ใช้ ราคาต่อยูนิตตามที่การค่าไฟกำหนด
                    </label>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="electricitybill3"
                      name="electricitybill"
                    />
                    <label class="form-check-label" for="electricitybill3">
                      รวมในค่าห้องแล้ว
                    </label>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="electricitybill4"
                      name="electricitybill"
                    />
                    <label class="form-check-label" for="electricitybill4">
                      โทรสอบถาม
                    </label>
                  </div>
                </div>
                <div class="col-2 mt-4">
                  <p>ระยะสัญญา</p>
                </div>
                <div class="col-10">
                  <div class="form-check mb-3 mt-3">
                    <div class="d-flex align-items-center" style="height: 36px">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="service"
                        id="service1"
                        checked
                      />
                      <div class="input-group ms-2">
                        <input
                          type="text"
                          class="form-control form-unit"
                          aria-label="Recipient's username"
                          aria-describedby="basic-addon2"
                          id="service-uint-1"
                        />
                        <span
                          class="input-group-text form-unit-text"
                          id="basic-addon2"
                          >เดือน</span
                        >
                      </div>
                    </div>
                  </div>
                  <div class="form-check mb-3 mt-3 d-none">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="service2"
                      name="service"
                    />
                    <label class="form-check-label" for="service2">
                      รวมในค่าห้องแล้ว
                    </label>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="service3"
                      name="service"
                    />
                    <label class="form-check-label" for="service3">
                      โทรสอบถาม
                    </label>
                  </div>
                </div>
                <div class="col-2 mt-4">
                  <p>ประกัน</p>
                </div>
                <div class="col-10">
                  <div class="form-check mb-3 mt-3">
                    <div class="d-flex align-items-center" style="height: 36px">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="guarantee"
                        id="guarantee1"
                        checked
                      />
                      <select
                        class="form-select ms-2"
                        aria-label="Default select example"
                        style="width: 200px"
                        id="guarantee-unit-1"
                      >
                        <option selected>ระบุเดือน</option>
                        <option value="1">1 เดือน</option>
                        <option value="2">2 เดือน</option>
                        <option value="3">3 เดือน</option>
                      </select>
                    </div>
                  </div>
                  <div class="d-flex align-items-center" style="height: 36px">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="guarantee"
                      id="guarantee2"
                    />
                    <div class="input-group ms-2">
                      <input
                        type="text"
                        class="form-control form-unit"
                        aria-label="Recipient's username"
                        aria-describedby="basic-addon2"
                        id="guarantee-unit-2"
                      />
                      <span
                        class="input-group-text form-unit-text"
                        id="basic-addon2"
                        >บาท</span
                      >
                    </div>
                    <label class="form-check-label ms-2" for="flexCheckDefault">
                      (เช่น 5,000 บาท หรือ 5,000 - 10,000 บาท)
                    </label>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="guarantee3"
                      name="guarantee"
                    />
                    <label class="form-check-label" for="guarantee3">
                      ไม่มีการเก็บค่าเช่าล่วงหน้า
                    </label>
                  </div>

                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="guarantee5"
                      name="guarantee"
                    />
                    <label class="form-check-label" for="guarantee5">
                      โทรสอบถาม
                    </label>
                  </div>
                </div>
                <div class="col-2 mt-4">
                  <p>ค่าโทรศัพท์</p>
                </div>
                <div class="col-10">
                  <div class="form-check mb-3 mt-3">
                    <div class="d-flex align-items-center" style="height: 36px">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="phonebill"
                        id="phonebill1"
                        checked
                      />
                      <label
                        class="form-check-label ms-2 me-2"
                        for="phonebill1"
                      >
                        คิดเป็นรายนาที
                      </label>
                      <div class="input-group ms-2">
                        <input
                          type="text"
                          class="form-control form-unit"
                          aria-label="Recipient's username"
                          aria-describedby="basic-addon2"
                          id="phonebill-unit-1"
                        />
                        <span
                          class="input-group-text form-unit-text"
                          id="basic-addon2"
                          >บาท/นาที</span
                        >
                      </div>

                      <select
                        class="form-select ms-2 d-none"
                        aria-label="Default select example"
                        style="width: 200px"
                        id="phonebill-unit-time"
                      >
                        <option selected>ระบุเวลา</option>
                        <option value="1 นาที">1 นาที</option>
                        <option value="10 นาที">10 นาที</option>
                        <option value="15 นาที">15 นาที</option>
                        <option value="20 นาที">20 นาที</option>
                        <option value="25 นาที">25 นาที</option>
                        <option value="30 นาที">30 นาที</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <div class="d-flex align-items-center" style="height: 36px">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="phonebill"
                        id="phonebill2"
                      />
                      <label
                        class="form-check-label ms-2 me-2"
                        for="phonebill2"
                      >
                        คิดเป็นรายครั้ง
                      </label>
                      <div class="input-group ms-2">
                        <input
                          type="text"
                          class="form-control form-unit"
                          aria-label="Recipient's username"
                          aria-describedby="basic-addon2"
                          id="phonebill-unit-2"
                        />
                        <span
                          class="input-group-text form-unit-text"
                          id="basic-addon2"
                          >บาท/ครั้ง</span
                        >
                      </div>
                    </div>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="phonebill3"
                      name="phonebill"
                    />
                    <label class="form-check-label" for="phonebill3">
                      ไม่มีโทรศัพท์
                    </label>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="phonebill4"
                      name="phonebill"
                    />
                    <label class="form-check-label" for="phonebill4">
                      โทรสอบถาม
                    </label>
                  </div>
                </div>
                <div class="col-2 mt-4">
                  <p>อินเทอร์เน็ต</p>
                </div>
                <div class="col-10">
                  <div class="form-check mb-3 mt-3">
                    <div class="d-flex align-items-center" style="height: 36px">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="Internet"
                        id="Internet1"
                        checked
                      />
                      <div class="input-group ms-2">
                        <input
                          type="text"
                          class="form-control form-unit"
                          aria-label="Recipient's username"
                          aria-describedby="basic-addon2"
                          id="Internet-unit-1"
                        />
                        <span
                          class="input-group-text form-unit-text"
                          id="basic-addon2"
                          >บาท/เดือน</span
                        >
                      </div>
                    </div>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="Internet2"
                      name="Internet"
                    />
                    <label class="form-check-label" for="Internet2">
                      รวมในค่าห้องแล้ว
                    </label>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="Internet3"
                      name="Internet"
                    />
                    <label class="form-check-label" for="Internet3">
                      ไม่มีอินเทอร์เน็ต
                    </label>
                  </div>
                  <div class="form-check mb-3 mt-3">
                    <input
                      class="form-check-input"
                      type="radio"
                      value=""
                      id="Internet4"
                      name="Internet"
                    />
                    <label class="form-check-label" for="Internet4">
                      โทรสอบถาม
                    </label>
                  </div>
                </div>
              </div>
              <div class="row mt-4">
                <h5><b>5. รายละเอียด</b></h5>
                <div class="col-12">
                  <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label"
                      >รายละเอียด ภาษาไทย</label
                    >
                    <textarea
                      class="form-control"
                      id="details"
                      rows="5"
                    ></textarea>
                  </div>
                </div>
              </div>
              <div class="row mt-4">
                <h5><b>6. รูปภาพ</b></h5>
                <div class="col-12">
                  <div class="mb-3">
                    <label for="exampleFormControlTextarea1" class="form-label"
                      >อัพโหลด รูปภาพ</label
                    >
                    <input
                      type="file"
                      id="photo"
                      class="form-control"
                      multiple
                      accept="image/*"
                    />
                    <div class="holder row">
                    </div>
                    <div class="imgEdit row">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row mt-4">
                <h5><b>7. ข้อมูลสำหรับติดต่อ</b></h5>
                <div class="row">
                  <div class="col-6">
                    <div class="mb-3">
                      <label for="exampleFormControlInput1" class="form-label"
                        >ชื่อผู้ดูแล</label
                      >
                      <input
                        type="email"
                        class="form-control"
                        id="contactInformationName"
                      />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-6">
                    <div class="mb-3">
                      <label for="exampleFormControlInput1" class="form-label"
                        >เบอร์ติดต่อ</label
                      >
                      <input
                        type="email"
                        class="form-control"
                        id="contactInformationTel"
                      />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-6">
                    <div class="mb-3">
                      <label for="exampleFormControlInput1" class="form-label"
                        >Email</label
                      >
                      <input
                        type="email"
                        class="form-control"
                        id="contactInformationEmail"
                      />
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-6">
                    <div class="mb-3">
                      <label for="exampleFormControlInput1" class="form-label"
                        >Line ID</label
                      >
                      <input
                        type="email"
                        class="form-control"
                        id="contactInformationLineID"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div class="row mt-4 mb-4">
                <div class="d-flex justify-content-sm-start w-100">
                  <button
                    type="button"
                    class="btn btn-save text-white ps-5 pe-5"
                  >
                    ลงประกาศ
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include '../component/footer.php';?>


    <script src="../assets/js/pages/listings.js"></script>
  </body>
</html>
